## Chapter 3 -  Importing your WAF logs to Athena through AWS Kinesis for analysis

This section walks you to utilize AWS Kinesis as a real time delivery system for your WAF logs to Athena.

[Amazon Kinesis](https://aws.amazon.com/kinesis/) makes it easy to collect, process, and analyze real-time, streaming data so you can get timely insights and react quickly to new information. Amazon Kinesis offers key capabilities to cost-effectively process streaming data at any scale, along with the flexibility to choose the tools that best suit the requirements of your application. 

[Amazon Athena](https://aws.amazon.com/athena/) is an interactive query service that makes it easy to analyze data in Amazon S3 using standard SQL. Athena is serverless, so there is no infrastructure to manage, and you pay only for the queries that you run.

## Prerequisites
> Prepare an AWS account.

> Make sure your are in US East (N. Virginia), which short name is us-east-1.

> **Make sure you have a CloudFront distribution, and set the logging save in your S3 bucket to analyze the log.**

> If you don't have one you can create it here on our [Chapter 2 - Creating Cloudfront Distribution with WAF Security](2-cloudfrontdistribution.md) 


## Step by Step

### 	Enable WAF logs

Create WAF logs delivery stream by Kinesis

> -	If WAF is on CloudFront, the Kinesis log streaming should be created at **N.Virginia** Region

> - If WAF is on ALB, the Kinesis log streaming should be created at the region which ALB locate


- On Services menu, select [Kinesis](https://console.aws.amazon.com/kinesis/home).
- Click Create delivery stream


<p align="center">
    <img src="images/3-athena-1.png" width="80%" height="80%">
</p>


- Enter `aws-waf-logs-bootcamp` for the **Delivery stream name**, and pick **Direct PUT or other sources** for the Source, then click Next.

-

- On the next **Process records** page , keep settings as default (like the picture below), then click **Next**.

<p align="center">
    <img src="images/3-athena-3.png" width="80%" height="80%">
</p>

-

- On the next **Select a destination** , choose Amazon S3 as your destination.

<p align="center">
    <img src="images/3-athena-4.png" width="80%" height="80%">
</p>

- For the **S3 destination** , create new bucket with bucket name `waf-logs-bootcamp`, region **US East (N. Virginia)**, then click **Create S3 bucket**

<p align="center">
    <img src="images/3-athena-5.png" width="80%" height="80%">
</p>

- On the previous windows, click **Next**

-

- On the next **Configure settings** , under the S3 Compression and encryption pick **GZIP** as your S3 Compression and **Enabled** as your S3 Encryption

<p align="center">
    <img src="images/3-athena-6.png" width="80%" height="80%">
</p>

- Then under permissions, click on **Create new or choose**

    <img src="images/3-athena-7.png" width="50%" height="80%">


- Then pick **create a new IAM Role**, and enter `firehose_delivery_role_waf` as your Role Name

<p align="center">
    <img src="images/3-athena-8.png" width="80%" height="80%">
</p>

- Then back at the previous page, it will show that our IAM role has changed to the one we just created, so click **Next**

<p align="center">
    <img src="images/3-athena-9.png" width="80%" height="80%">
</p>

-

<p align="center">
    <img src="images/3-athena-10.png" width="80%" height="80%">
</p>

- On the next **Review** page , recheck your configurations and click **Create Delivery System**

- Wait until the status become Active, message Successfully created delivery stream will appeared.

<p align="center">
    <img src="images/3-athena-11.png" width="80%" height="80%">
</p>

###Enable Logging with WAF

- On Services menu, select **WAF & Shield**
- In left panel, select **Web ACL**, and select the **Name** of your Web ACLs
- In the click on tab **Logging**, click on **Enable Logging** below

<p align="center">
    <img src="images/3-athena-12.png" width="80%" height="80%">
</p>

- In Amazon Kinesis Data Firehose, select the `aws-waf-logs-bootcamp` you just create before and click **Create**

<p align="center">
    <img src="images/3-athena-13.png" width="80%" height="80%">
</p>

>From here your WAF Logs are already recorded to S3 by the kinesis data stream, the logs will appear on the S3 selected, in minutes according to your Website's Traffic

-----
-


- In Services menu, select AWS Glue
- At the left panel, click Database then click **Add database**
- In the dialogue shown, type your Database name and click Create

<p align="center">
    <img src="images/3-athena-14.png" width="80%" height="80%">
</p>

- In the left panel, select Crawler then click **Add Crawler**
- Enter your `waf_logs_crawler` as the name and click next

<p align="center">
    <img src="images/3-athena-15.png" width="80%" height="80%">
</p>

- On the next window pick **Data stores** as the crawler source type and click **next**

<p align="center">
    <img src="images/3-athena-16.png" width="80%" height="80%">
</p>

- On the next window pick **s3://waf-logs-bootcamp** (the S3 bucket you created together with your kinesis) as the name and click **next**

<p align="center">
    <img src="images/3-athena-17.png" width="80%" height="80%">
</p>

- Leave the Add another data store to **No** and click **next**

<p align="center">
    <img src="images/3-athena-18.png" width="80%" height="80%">
</p>

- On the next window choose **Create an IAM role**, give a name for the IAM role and click **Next** 

	> for this tutorial I am using `waf-logs-bootcamp`

<p align="center">
    <img src="images/3-athena-19.png" width="80%" height="80%">
</p>

- For the schedule of the crawler, just leave it at default at **Run on Demand** and click **Next**

<p align="center">
    <img src="images/3-athena-20.png" width="80%" height="80%">
</p>

- For the crawler output, just pick the database `waf_logs_bootcamp` that we created before, and click **Next**, 

<p align="center">
    <img src="images/3-athena-21.png" width="80%" height="80%">
</p>

- On the review page, recheck your configurations, and click **Finish**

<p align="center">
    <img src="images/3-athena-22.png" width="80%" height="80%">
</p>

- After that click **Run it now** on the top message shown on the notifications that showed up

<p align="center">
    <img src="images/3-athena-23.png" width="80%" height="80%">
</p>

-

- On Services menu, select Athena, then click Get Started

<p align="center">
    <img src="images/3-athena-24.png" width="80%" height="80%">
</p>

- Add a Glue ETL job to modify the log format to parquet

<p align="center">
    <img src="images/3-athena-25.png" width="80%" height="80%">
</p>

- Give your job a name for this tutorial I am using `waf-logs-to-parquet-bootcamp`
- The IAM role = **AWSGlueServiceRoleDefault**
- Script file name = **waf-logstoparquet-bootcamp**
- Under Advanced Properties, job bookmark as **Enable**

<p align="center">
    <img src="images/3-athena-27.png" width="80%" height="80%">
</p>

<p align="center">
    <img src="images/3-athena-28.png" width="80%" height="80%">
</p>

- Select your data source , in this tutorial it is `waf_logs_bootcamp`, and then click **Next**

<p align="center">
    <img src="images/3-athena-29.png" width="80%" height="80%">
</p>


- Open a new window and create a new S3 bucket to keep the logs that is formatted to parquet, in this tutorial it is `waf_logs_bootcamp_parquet`

<p align="center">
    <img src="images/3-athena-30.png" width="80%" height="80%">
</p>

- Back to the AWS Glue Page, choose a data target, **pick create tables in your data target**, keep the data store in **Amazon S3** and pick the Target path as `waf_logs_bootcamp_parquet` bucket, then click **Next**

<p align="center">
    <img src="images/3-athena-31.png" width="80%" height="80%">
</p>

- On the next windows, click **Save jobs and edit script**

<p align="center">
    <img src="images/3-athena-32.png" width="80%" height="80%">
</p>

- On the next windows, click on **Save** and then  **Run jobs**

<p align="center">
    <img src="images/3-athena-33.png" width="80%" height="80%">
</p>

- Back on the Jobs left panel, check if it has succeded running the job

<p align="center">
    <img src="images/3-athena-34.png" width="80%" height="80%">
</p>

- Create a new crawler to crawl our newly formatted into parquet data, In the left panel, select Crawler then click **Add Crawler**
- Enter your `waf_logs_crawler_parquet` as the name and click next

<p align="center">
    <img src="images/3-athena-36.png" width="80%" height="80%">
</p>

- On the next window pick **Data stores** as the crawler source type and click **next**

<p align="center">
    <img src="images/3-athena-37.png" width="80%" height="80%">
</p>

- On the next window pick **s3://waf-logs-bootcamp-parquet** (the S3 bucket you created with your job) as the name and click **next**

<p align="center">
    <img src="images/3-athena-38.png" width="80%" height="80%">
</p>

- Leave the Add another data store to **No** and click **next**

<p align="center">
    <img src="images/3-athena-18.png" width="80%" height="80%">
</p>

- On the next window choose **Create an IAM role**, give a name for the IAM role and click **Next** 

	> for this tutorial I am using `waf-logs-bootcamp`

<p align="center">
    <img src="images/3-athena-19.png" width="80%" height="80%">
</p>

- For the schedule of the crawler, just leave it at default at **Run on Demand** and click **Next**

<p align="center">
    <img src="images/3-athena-20.png" width="80%" height="80%">
</p>

- For the crawler output, just pick the database `waf_logs_bootcamp` that we created before, and click **Next**, 

<p align="center">
    <img src="images/3-athena-21.png" width="80%" height="80%">
</p>

- On the review page, recheck your configurations, and click **Finish**

<p align="center">
    <img src="images/3-athena-22.png" width="80%" height="80%">
</p>


- Check the crawler status and the new table has been created

<p align="center">
    <img src="images/3-athena-40.png" width="80%" height="80%">
</p>

<p align="center">
    <img src="images/3-athena-41.png" width="80%" height="80%">
</p>

-	Retry to query waf logs with parquet format on Athena service page

<p align="center">
    <img src="images/3-athena-42.png" width="80%" height="80%">
</p>


## Clean Up

To delete the AWS resources after testing purposes, perform the tasks below in order:

- Delete Your AWS Kinesis delivery streams
- Delete Your AWS Glue, database, crawler, and job
- Delete Your Cloudfront and WAF settings

## Conclusion

Congratulations! You now have learned how to:

- Import your WAF logs to Athena for Analysis in Quicksight

Click Here to learn more and continue our WAF series <br>
<h3 align="center">
[Go to 4. Creating a Analysis Dashboard with Quicksight](4-quicksight.md)
</h3>
