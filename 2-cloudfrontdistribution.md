## Chapter 2 -  Creating Cloudfront Distribution with WAF Security

This section walks you to utilize cloudfront as a fast cdn to your static website

[CloudFront](https://aws.amazon.com/cloudfront/) is a fast content delivery network(CDN) service that securely delivers data, videos, applications, and APIs on a global scale with low latency, high transfer speeds, all within a developer-friendly environment. You can read more about CloudFront in one of our previous blogs. Amazon CloudFront is built on Amazon’s highly reliable infrastructure, providing a cost-effective, fast and reliable way to provide service to your customers.
In this blog, I’ll be sharing my experience of configuring AWS CloudFront with the data origin in S3.

### Prerequisites
> AWS Account

> **A Website or a source for your content**

> If you don't have one you can create it here on our [Chapter 1 - Hosting a Static Website on Amazon S3](1-statichosting.md) 


## Step by Step

### Create a cloudfront distribution

- Open the [AWS Cloudfront console](https://console.aws.amazon.com/cloudfront/home?region=us-east-1).
- Choose **Create Distribution**.
- Pick **Get Started** on **Web**.
- For Origin Domain Name, choose `<YOURBUCKETNAME>.s3-website-<REGION>.amazonaws.com.`
- Set on for Logging, choose `<YOURBUCKETNAME>.s3-website-<REGION>.amazonaws.com.`

<p align="center">
    <img src="images/2-cloud-1.png" width="80%" height="80%">
</p>
<p align="center">
    <img src="images/2-cloud-10.png" width="80%" height="80%">
</p>

- For the rest, leave everything as it is and click **Create Distribution**

### Create a WAF Access Control List
 
 - Open the [AWS WAF & Shield](https://console.aws.amazon.com/wafv2/home).
- Choose **Create web ACL**.
- Pick **Get Started** on **Web**.
- For **Web ACL Name**, enter `bootcamp-acl`
- For **Region**, enter `Global (Cloudfront)`
- As for **AWS resource to associate**, pick your cloudfront distribution
- and click **Next**

<p align="center">
    <img src="images/2-cloud-2.png" width="80%" height="80%">
</p>

- On the next page, **Create conditions** page, you specify the filters that you want to use to allow or block requests that are forwarded your CloudFront distributions.

- Under **Geo Match condition**, pick **Create condition**. It will open the **Create geo match condition** page.

<p align="center">
    <img src="images/2-cloud-3.png" width="80%" height="80%">
</p>

- On the **Create geo match condition** page, enter `bootcamp-acl-geo` as your **Name**
- Under Filter Settings, pick **Taiwan** as your **Location** , then click **Add Location**, then proceed to **Create**

<p align="center">
    <img src="images/2-cloud-4.png" width="80%" height="80%">
</p>

- After that back to the previous page, it will show that your geo match conditions is created

<p align="center">
    <img src="images/2-cloud-5.png" width="80%" height="80%">
</p>

- Scroll down and click **next**

- On the next **Create rules** page, choose **Create rule**.

<p align="center">
    <img src="images/2-cloud-6.png" width="80%" height="80%">
</p>

- Enter `bootcamp-acl-rule` as the **name**, we are going to count international reaches of our resource, so we choose 

<p align="center">
    <img src="images/2-cloud-7.png" width="80%" height="80%">
</p>

- We pick `does not` , `originate from a geographic location in` , `bootcamp-acl-geo` ,  and then click **create** ,  it will show that our rules is created, then we pick **count** as the action, then click **Review and create**

<p align="center">
    <img src="images/2-cloud-8.png" width="80%" height="80%">
</p>

- And then we click **Confirm and create**

	>FINAL NOTES:
	Your CDN Will be available when Cloudfront is already deployed
	<p align="center">
    <img src="images/2-cloud-9.png" width="80%" height="80%">
	</p>



## Clean Up

To delete the AWS resources, perform the tasks below in order:

- Disable the cloudfront distribution, then after it is disabled, **Delete** the distribution.
- Delete the WAF ACL `bootcamp-acl`.

## Conclusion

Congratulations! You now have learned how to:

- Utilize AWS Cloudfront as your CDN, and created a rule to count how much request from outside of taiwan that your resource received.

Click Here to learn more and continue our WAF series <br>
<h3 align="center">
[Go to 3. Importing your WAF logs to Athena through AWS Kinesis for analysis](3-importathena.md)
</h3>
