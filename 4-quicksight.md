## Chapter 4 -  Creating a Analysis Dashboard with Quicksight

This section walks you to the process of creating an analysis dashboard with quicksight.

[Amazon QuickSight](https://aws.amazon.com/quicksight/) is a fast, cloud-powered business intelligence service that makes it easy to deliver insights to everyone in your organization.

As a fully managed service, QuickSight lets you easily create and publish interactive dashboards that include ML Insights. Dashboards can then be accessed from any device, and embedded into your applications, portals, and websites.

With our Pay-per-Session pricing, QuickSight allows you to give everyone access to the data they need, while only paying for what you use.

## Prerequisites
> Prepare an AWS account.

> Make sure your are in US East (N. Virginia), which short name is us-east-1.

> **Data in Athena that we will be analyzing**

> If you don't have one you can create it here on our [Chapter 3 - Importing your WAF logs to Athena through AWS Kinesis for analysis](3-importathena.md) 


## Step by Step

### Configure Permissions in AWS Quicksight

- Open the [AWS Quicksight](https://quicksight.aws.amazon.com).
- On the top right corner pick **Manage Quicksight**.

<p align="center">
    <img src="images/4-quicksight-1.png" width="80%" height="80%">
</p>
 
- On the left panel pick **Security & Permissions**, and then pick **Add or remove**

<p align="center">
    <img src="images/4-quicksight-2.png" width="80%" height="80%">
</p>

- Amazon S3 panel there is a **Details** button, click it as we need to let Quicksight access or bucket.

<p align="center">
    <img src="images/4-quicksight-3.png" width="80%" height="80%">
</p>

- Then pick, **Select S3 Buckets** to select your S3 bucket containing the data to be visualized

<p align="center">
    <img src="images/4-quicksight-4.png" width="80%" height="80%">
</p>

- Tick the buckets containing, your **AWS Athena Query Results**, your **WAF Logs in original** and **parquet** format, then click **Select Buckets** , then on the previous window, click **Update**

<p align="center">
    <img src="images/4-quicksight-5.png" width="80%" height="80%">
</p>

- On the left corner, click on the **Quicksight logo** to go to the home page, and then click on New Analysis

<p align="center">
    <img src="images/4-quicksight-6.png" width="80%" height="80%">
</p>

- On the next page, click on **New data set**

<p align="center">
    <img src="images/4-quicksight-7.png" width="80%" height="80%">
</p>

- On the next page, pick **Athena** , then pick **WAF logs** as your data source name, then click **Create data source**

<p align="center">
    <img src="images/4-quicksight-8.png" width="80%" height="80%">
</p>

- On the new windows that showed up, click on **Use custom SQL**

<p align="center">
    <img src="images/4-quicksight-9.png" width="80%" height="80%">
</p>

- Then on the new windows that showed up, click on **Edit/Preview data**
- Paste this SQL on the textarea of *Enter SQL here...*

```
with d as (select
    waf.timestamp,
        waf.terminatingruleid,
        waf.terminatingruletype,
        waf.action,
        waf.httpsourcename,
        waf.httpsourceid,
        waf.HTTPREQUEST.clientip as clientip,
        waf.HTTPREQUEST.country as country,
        waf.HTTPREQUEST.httpMethod as httpMethod,
        waf.HTTPREQUEST.uri as URI,
        map_agg(f.name,f.value) as kv
    from {DB_NAME.TABLE_NAME} waf,
    UNNEST(waf.httprequest.headers) as t(f)
    group by 1,2,3,4,5,6,7,8,9,10)
    select d.timestamp,
        d.terminatingruleid,
        d.terminatingruletype,
        d.action,
        d.httpsourcename,
        d.httpsourceid,
        d.clientip,
        d.country,
        d.httpMethod,
        d.URI,
        d.kv['User-Agent'] as Useragent,
        d.kv['Referer'] as Referer
    from d
```
>*Remember to edit DB_NAME to your database name & also TABLE_NAME to your database's table name*

<p align="center">
    <img src="images/4-quicksight-11.png" width="80%" height="80%">
</p>

- Then on the new windows that showed up, click on **Edit/Preview data**

<p align="center">
    <img src="images/4-quicksight-10.png" width="80%" height="80%">
</p>

- Modify the attribute of timestamp to “Date”

<p align="center">
    <img src="images/4-quicksight-12.png" width="80%" height="80%">
</p>


- Add a calculated field to do WAF rule ID mapping *(Because the WAF rule in WAF logs only record the rule ID)*

<p align="center">
    <img src="images/4-quicksight-13.png" width="80%" height="80%">
</p>

- You can map the rule name and ID on WAF service page
<p align="center">
    <img src="images/4-quicksight-14.png" width="80%" height="80%">
</p>
	Example: 

    ```
ifelse(
terminatingruleid = '5d0394de-32f0-4438-b242-f0fbed616f82','speedpost-ip-whitelist',
terminatingruleid = '41baf0c3-e8e1-4324-82ba-3a0f790fafb9','10k-5mins-rate-limit',
terminatingruleid = 'e063e73b-7baf-44a6-9b2b-f85826431b46','generic-enforce-csrf',
terminatingruleid = '80189347-5cbd-42c1-9853-11261049a729','generic-detect-rfi-lfi-traversal',
terminatingruleid = 'a0090983-ca59-4585-9872-10a776a952d7','generic-detect-ssi',
terminatingruleid = '63e4cc54-f5e1-4757-83f6-c0279400d99c','generic-mitigate-sqli',
terminatingruleid = '3b44ddbd-9838-4378-8523-e5ddb00ccc6c','generic-mitigate-xss',
'Default-Action'
)
	```
	
<p align="center">
    <img src="images/4-quicksight-15.png" width="80%" height="80%">
</p>

- And then click on the **Save & visualize** button on the top right
	
<p align="center">
    <img src="images/4-quicksight-16.png" width="80%" height="80%">
</p>




### Designing Quicksight Dashboard

We'll upload the web file to the S3 bucket just created via AWS CLI.

<p align="center">
    <img src="images/4-quicksight-17.png" width="80%" height="80%">
</p>

A.	Top request IPs

<p align="center">
    <img src="images/4-quicksight-18.png" width="80%" height="80%">
</p>


<p align="center">
    <img src="images/4-quicksight-19.png" width="80%" height="80%">
</p>


B.	Rate of WAF rules

<p align="center">
    <img src="images/4-quicksight-20.png" width="80%" height="80%">
</p>




C.	Top request URI

<p align="center">
    <img src="images/4-quicksight-20.png" width="80%" height="80%">
</p>


D.	Count of all request

<p align="center">
    <img src="images/4-quicksight-22.png" width="80%" height="80%">
</p>


E.	Count of blocked request

<p align="center">
    <img src="images/4-quicksight-23.png" width="80%" height="80%">
</p>

<p align="center">
    <img src="images/4-quicksight-24.png" width="80%" height="80%">
</p>

<p align="center">
    <img src="images/4-quicksight-25.png" width="80%" height="80%">
</p>

F. Count of request by time

<p align="center">
    <img src="images/4-quicksight-26.png" width="80%" height="80%">
</p>

G. Adjust the visual position and size

<p align="center">
    <img src="images/4-quicksight-27.png" width="80%" height="80%">
</p>

### Set Up Filters

A.	View IP address

<p align="center">
    <img src="images/4-quicksight-28.png" width="80%" height="80%">
</p>

<p align="center">
    <img src="images/4-quicksight-29.png" width="80%" height="80%">
</p>

B. WAF Rules

> Try to do it by yourself ;)

C. Action

- We are going to use Custom filter because there has no block action appearing yet.
- We can easily accomplish the custom request by enabling this filter. 

<p align="center">
    <img src="images/4-quicksight-30.png" width="80%" height="80%">
</p>

<p align="center">
    <img src="images/4-quicksight-31.png" width="80%" height="80%">
</p>

D. Originated country

> Try to do it by yourself ;)

E. User Agent

> Try to do it by yourself ;)

F. Disable all the Filters by default

<p align="center">
    <img src="images/4-quicksight-32.png" width="80%" height="80%">
</p>

G. Publish your dashboard

<p align="center">
    <img src="images/4-quicksight-33.png" width="80%" height="80%">
</p>

<p align="center">
    <img src="images/4-quicksight-34.png" width="80%" height="80%">
</p>

## Conclusion

Congratulations! You now have learned how to:

- Create a data visualization of your WAF Logs statistics

<h3 align="center">
[Click here to go back to list of contents](README.md)
</h3>
