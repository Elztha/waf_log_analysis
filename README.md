# WAF Logs Analysis

## Table of Contents

  - [Introduction](#Introduction)
  - [Scenario](#Scenario)
  - [Overview](#Overview)
  - [Prerequisites](#Prerequisites)
  - [Step by Step](#Step-by-Step)
    - [Hosting a simple static website with AWS S3](1-statichosting.md)
    - [Create a Cloudfront Distribution with WAF for your website](2-cloudfrontdistribution.md)
    - [Import WAF statistics to Amazon Athena for analysis](3-importathena.md)
    - [Start analyzing with QuickSight](4-quicksight.md)
  - [Conclusion](#Conclusion)



## Introduction

[Amazon Athena](https://aws.amazon.com/tw/athena/) is an interactive query service that makes it easy to analyze data in Amazon S3 using standard SQL. Athena is serverless, so there is no infrastructure to manage, and you pay only for the queries that you run. Use it with [AWS Quicksight](https://aws.amazon.com/tw/quicksight/), a fast, cloud-powered business analytics service that makes it easy to build visualizations, perform ad-hoc analysis, and quickly get business insights from your data. Using our cloud-based service you can easily connect to your data, perform advanced analysis, and create stunning visualizations and rich dashboards.

## Scenario
Massive data could either mean more cost, as well means more opportunity, only if you have enough knowledge to discover the meaning inside it. In today’s fast-paced business world, we need more active information source to predict our customers’ potential need or find out the hidden problem, big data can help you to solve it out.

## Overview

<p align="center">
  <img src="images/waflogs.png"  width="60%">
</p>

  Here is a rough structure diagram of the infrastructure that we will set for our simple website, together they perform a "real-time" data analysis of traffic in your websites captured through WAF and analyzed with Quicksight and AWS Glue.


## Prerequisites
> Prepare an AWS account.

> Make sure your are in US East (N. Virginia), which short name is us-east-1.

> *Other Prequisites Resources are re-specified on each step in this tutorial*


## Step by Step

 - [Hosting a simple static website with AWS S3](1-statichosting.md)
 
 	In this part we will be hosting a simple static website through S3 to be used later distributed and filtered with Cloudfront and WAF Rules
 - [Create a Cloudfront Distribution with WAF for your website](2-cloudfrontdistribution.md)

 In this part we will be creating Cloudfront Distribution with WAF Rules for our simple static website.
 
 - [Import WAF statistics to Amazon Athena for analysis](3-importathena.md)

 In this part we will be importing our Cloudfront and WAF Logs to Amazon Athena through Amazon Kinesis and we will also use AWS Glue to perform some jobs to our Logs data
 
 - [Start analyzing with QuickSight](4-quicksight.md)

 In this part we will be walking through how you make a visualization of your data, while also creating a dashboard for the whole data analysis
